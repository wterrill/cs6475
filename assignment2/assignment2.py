# ASSIGNMENT 2
# William L Terrill
# GTID: 903050269

import cv2
import numpy as np
import scipy as sp

""" Assignment 2 - Basic Image Input / Output / Simple Functionality

This file has a number of functions that you need to fill out in order to
complete the assignment. Please write the appropriate code, following the
instructions on which functions you may or may not use.

GENERAL RULES:
    1. DO NOT INCLUDE code that saves, shows, displays, writes the image that
    you are being passed in. Do that on your own if you need to save the images
    but the functions should NOT save the image to file. (This is a problem
    for us when grading because running 200 files results a lot of images being
    saved to file and opened in dialogs, which is not ideal). Thanks.

    2. DO NOT import any other libraries aside from the three libraries that we
    provide. You may not import anything else, you should be able to complete
    the assignment with the given libraries (and in most cases without them).

    3. DO NOT change the format of this file. Do not put functions into classes,
    or your own infrastructure. This makes grading very difficult for us. Please
    only write code in the allotted region.
"""

def numberOfPixels(image):
    """ This function returns the number of pixels in a grayscale image.

    Note: A grayscale image has one dimension as covered in the lectures. You
    DO NOT have to account for a color image.

    You may use any / all functions to obtain the number of pixels in the image.

    Args:
        image (numpy.ndarray): A grayscale image represented in a numpy array.

    Returns:
        int: The number of pixels in an image.
    """
    # WRITE YOUR CODE HERE.
    
    num_pixels = int(np.prod(image.shape))
    print ""
    print "the number of pixels is %d" % num_pixels
    

    #test result
    num_pixels_test = image.shape[0] * image.shape[1]
    if num_pixels == num_pixels_test:
        print "TEST PASSED. The number of pixels matched between two methods"
    else:
        print"TEST FAILED. The number of pixels did not match between two methods"
    return num_pixels


    # END OF FUNCTION.

def averagePixel(image):
    """ This function returns the average color value of a grayscale image.

    Assignment Instructions: In order to obtain the average pixel, add up all
    the pixels in the image, and divide by the total number of pixels. We advise
    that you use the function you wrote above to obtain the number of pixels!

    You may not use numpy.mean or numpy.average. All other functions are fair
    game.

    Args:
        image (numpy.ndarray): A grayscale image represented in a numpy array.

    Returns:
        int: The average pixel in the image (Range of 0-255).
    """
    # WRITE YOUR CODE HERE.
    num_pixels = numberOfPixels(image)
    array_dimensions = image.shape
    total = 0
    for x in range (0,array_dimensions[0]):
        for y in range(0,array_dimensions[1]):
            total = image[x][y] + total
    mean = total / num_pixels
    


    #test result
    print ""
    print "here is the average calculated by iteration"
    print mean
    print "here is the average with OpenCV"
    print int(cv2.mean(image)[0])
    if int(cv2.mean(image)[0]) == mean:
        print "TEST PASSED. Means matched between two methods"
    else:
        print "TEST FAILED. Means did not match between two methods"
    return int(mean)

    # END OF FUNCTION.

def convertToBlackAndWhite(image):
    """ This function converts a grayscale image to black and white.

    Assignment Instructions: Iterate through every pixel in the image. If the
    pixel is strictly greater than 128, set the pixel to 255. Otherwise, set the
    pixel to 0. You are essentially converting the input into a 1-bit image, as 
    we discussed in lecture, it is a 2-color image.

    You may NOT use any thresholding functions provided by OpenCV to do this.
    All other functions are fair game.

    Args:
        image (numpy.ndarray): A grayscale image represented in a numpy array.

    Returns:
        numpy.ndarray: The black and white image.
    """
    # WRITE YOUR CODE HERE.
    array_dimensions = image.shape
    total = 0
    for x in range (0,array_dimensions[0]):
        for y in range(0,array_dimensions[1]):
            if image[x][y] > 128:
                image[x][y] = 255
            else:
                image[x][y] = 0
    
    cv2.imwrite('3BlackAndWhite.png',image)

    #test result
    zeros = np.count_nonzero(image) 
    ones = (image==0).sum()
    print ""
    print "number of pixels = 0 is: %d" % zeros
    print "number of pixels = 255 is: %d" % ones
    print "number of pixels is:  %d" % numberOfPixels(image)
    if zeros + ones == numberOfPixels(image):
        print "TEST PASSED. All pixels are either 0 or 255"
    else:
        print "TEST FAILED. All pixels are not either 0 or 255"
    return image


    # END OF FUNCTION.

def averageTwoImages(image1, image2):
    """ This function averages the pixels of the two input images.

    Assignment Instructions: Obtain the average image by adding up the two input
    images on a per pixel basis and dividing them by two.

    You may use any / all functions to obtain the average image output.

    Note: You may assume image1 and image2 are the SAME size.

    Args:
        image1 (numpy.ndarray): A grayscale image represented in a numpy array.
        image2 (numpy.ndarray): A grayscale image represented in a numpy array.

    Returns:
        numpy.ndarray: The average of image1 and image2.

    """
    # WRITE YOUR CODE HERE.
    array_dimensions = image1.shape
    average_array = np.zeros(array_dimensions)

    for x in range (0,array_dimensions[0]):
        for y in range(0,array_dimensions[1]):
            average_array[x][y] = (int(image1[x][y]) + int(image2[x][y]))/2
            #print image1[x][y] 
            #print image2[x][y]
            #print "average" 
            #print average_array[x][y] 

    cv2.imwrite('4averageTwoImages.png',average_array)

    #Test Result
    testx = 133
    testy = 145
    test_spot1 = (image1[testx][testy] + image2[testx][testy])/2
    check_spot1 = average_array[testx][testy]

    testx = 700
    testy = 1500
    test_spot2 = (image1[testx][testy] + image2[testx][testy])/2
    check_spot2 = average_array[testx][testy]

    testx = 1300
    testy = 2000
    test_spot3 = (image1[testx][testy] + image2[testx][testy])/2
    check_spot3 = average_array[testx][testy]

    print ""
    print "test spot 1 value = %d" % test_spot1
    print "check spot 1 value = %d" % check_spot1
    print "test spot 2 value = %d" % test_spot2
    print "check spot 2 value = %d" % check_spot2
    print "test spot 3 value = %d" % test_spot3
    print "check spot 3 value = %d" % check_spot3
    if (test_spot1 == check_spot1 and test_spot2 == check_spot2 and test_spot3 == check_spot3):
        print "TEST PASSED. Three test spot averaged out to the correct values"
    else:
        print "TEST FAILED. Three test spots did not average out to the correct values."

    return average_array

    # END OF FUNCTION.

def flipHorizontal(image):
    """ This function flips the input image across the horizontal axis.

    Assignment Instructions: Given an input image, flip the image on the
    horizontal axis. This can be interpreted as switching the first and last
    column of the image, the second and second to last column, and so on.

    You may use any / all functions to flip the image horizontally.

    Args:
        image (numpy.ndarray): A grayscale image represented in a numpy array.

    Returns:
        numpy.ndarray: The horizontally flipped image.

    """
    # WRITE YOUR CODE HERE.
    array_dimensions = image.shape
    flipped_array = np.zeros(array_dimensions)
    for x in range (0,array_dimensions[0]):
        for y in range(0,array_dimensions[1]):
            flipped_array[array_dimensions[0]-1-x][y] = image[x][y]
    
    cv2.imwrite('5flipHorizontal.png',image)
    #Test Result
    testx = 143
    testy = 143
    test_spot1 = image[testx][testy]
    check_spot1 = flipped_array[testx][testx]
    flipped_spot1 = flipped_array[array_dimensions[0]-1-testx][testy]
    testx = 600
    testy = 1504
    test_spot2 = image[testx][testy]
    check_spot2 = flipped_array[testx][testx]
    flipped_spot2 = flipped_array[array_dimensions[0]-1-testx][testy]
    testx = 1364
    testy = 2015
    test_spot3 = image[testx][testy]
    check_spot3 = flipped_array[testx][testx]
    flipped_spot3 = flipped_array[array_dimensions[0]-1-testx][testy]
    print ""
    print "test spot value = %d" % test_spot1
    print "check spot value = %d" % check_spot1
    print "flipped spot value = %d" % flipped_spot1
    print "test spot value = %d" % test_spot2
    print "check spot value = %d" % check_spot2
    print "flipped spot value = %d" % flipped_spot2
    print "test spot value = %d" % test_spot3
    print "check spot value = %d" % check_spot3
    print "flipped spot value = %d" % flipped_spot3
    if (test_spot1  != check_spot1 and test_spot1 == flipped_spot1 and
        test_spot2  != check_spot2 and test_spot2 == flipped_spot2 and
        test_spot3  != check_spot3 and test_spot3 == flipped_spot3)  :
        print "TEST PASSED. Three spots checked against flipped image were correct."
    else:
        print "TEST FAILED. Three spots checked against flipped image were not correct."
        
    return flipped_array


    # END OF FUNCTION.

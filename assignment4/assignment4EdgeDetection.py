#code by William Terrill
#CS 6475 - Computational Photography
import numpy as np
import cv2
import unittest

from assignment2 import convertToBlackAndWhite
from assignment4 import computeGradient

image = cv2.imread("img1.JPG",cv2.CV_LOAD_IMAGE_GRAYSCALE)
#cv2.imshow('hmm',image)
#cv2.waitKey()
#image=cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image,(5,5),0)
#cv2.imshow('blurred.png',blurred)
#cv2.waitKey()

kernelx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernely = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
gradientx = computeGradient(image,kernelx)
gradienty = computeGradient(image,kernely)
dim = gradientx.shape
edges = np.zeros(dim)
edges = np.add(np.square(gradientx), np.square(gradienty))
edges = np.sqrt(edges)
output = convertToBlackAndWhite(edges,64)


cv2.imwrite('img1-low.png',edges)


image = cv2.imread("img2.JPG",cv2.CV_LOAD_IMAGE_GRAYSCALE)
#cv2.imshow('hmm',image)
#cv2.waitKey()
#image=cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image,(5,5),0)
#cv2.imshow('blurred.png',blurred)
#cv2.waitKey()

kernelx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernely = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
gradientx = computeGradient(image,kernelx)
gradienty = computeGradient(image,kernely)
dim = gradientx.shape
edges = np.zeros(dim)
edges = np.add(np.square(gradientx), np.square(gradienty))
edges = np.sqrt(edges)
output = convertToBlackAndWhite(edges,64)


cv2.imwrite('img2-low.png',edges)


image = cv2.imread("img3.JPG",cv2.CV_LOAD_IMAGE_GRAYSCALE)
#cv2.imshow('hmm',image)
#cv2.waitKey()
#image=cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image,(5,5),0)
#cv2.imshow('blurred.png',blurred)
#cv2.waitKey()

kernelx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernely = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
gradientx = computeGradient(image,kernelx)
gradienty = computeGradient(image,kernely)
dim = gradientx.shape
edges = np.zeros(dim)
edges = np.add(np.square(gradientx), np.square(gradienty))
edges = np.sqrt(edges)
output = convertToBlackAndWhite(edges,64)


cv2.imwrite('img3-low.png',edges)

#8*********************LOW**************************************************

image = cv2.imread("img1.JPG",cv2.CV_LOAD_IMAGE_GRAYSCALE)
#cv2.imshow('hmm',image)
#cv2.waitKey()
#image=cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image,(5,5),0)
#cv2.imshow('blurred.png',blurred)
#cv2.waitKey()

kernelx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernely = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
gradientx = computeGradient(image,kernelx)
gradienty = computeGradient(image,kernely)
dim = gradientx.shape
edges = np.zeros(dim)
edges = np.add(np.square(gradientx), np.square(gradienty))
edges = np.sqrt(edges)
output = convertToBlackAndWhite(edges,128)


cv2.imwrite('img1-med.png',edges)


image = cv2.imread("img2.JPG",cv2.CV_LOAD_IMAGE_GRAYSCALE)
#cv2.imshow('hmm',image)
#cv2.waitKey()
#image=cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image,(5,5),0)
#cv2.imshow('blurred.png',blurred)
#cv2.waitKey()

kernelx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernely = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
gradientx = computeGradient(image,kernelx)
gradienty = computeGradient(image,kernely)
dim = gradientx.shape
edges = np.zeros(dim)
edges = np.add(np.square(gradientx), np.square(gradienty))
edges = np.sqrt(edges)
output = convertToBlackAndWhite(edges,128)


cv2.imwrite('img2-med.png',edges)


image = cv2.imread("img3.JPG",cv2.CV_LOAD_IMAGE_GRAYSCALE)
#cv2.imshow('hmm',image)
#cv2.waitKey()
#image=cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image,(5,5),0)
#cv2.imshow('blurred.png',blurred)
#cv2.waitKey()

kernelx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernely = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
gradientx = computeGradient(image,kernelx)
gradienty = computeGradient(image,kernely)
dim = gradientx.shape
edges = np.zeros(dim)
edges = np.add(np.square(gradientx), np.square(gradienty))
edges = np.sqrt(edges)
output = convertToBlackAndWhite(edges,128)


cv2.imwrite('img3-med.png',edges)

#*********************MED***************************************

image = cv2.imread("img1.JPG",cv2.CV_LOAD_IMAGE_GRAYSCALE)
#cv2.imshow('hmm',image)
#cv2.waitKey()
#image=cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image,(5,5),0)
#cv2.imshow('blurred.png',blurred)
#cv2.waitKey()

kernelx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernely = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
gradientx = computeGradient(image,kernelx)
gradienty = computeGradient(image,kernely)
dim = gradientx.shape
edges = np.zeros(dim)
edges = np.add(np.square(gradientx), np.square(gradienty))
edges = np.sqrt(edges)
output = convertToBlackAndWhite(edges,192)


cv2.imwrite('img1-high.png',edges)


image = cv2.imread("img2.JPG",cv2.CV_LOAD_IMAGE_GRAYSCALE)
#cv2.imshow('hmm',image)
#cv2.waitKey()
#image=cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image,(5,5),0)
#cv2.imshow('blurred.png',blurred)
#cv2.waitKey()

kernelx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernely = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
gradientx = computeGradient(image,kernelx)
gradienty = computeGradient(image,kernely)
dim = gradientx.shape
edges = np.zeros(dim)
edges = np.add(np.square(gradientx), np.square(gradienty))
edges = np.sqrt(edges)
output = convertToBlackAndWhite(edges,192)


cv2.imwrite('img2-high.png',edges)


image = cv2.imread("img3.JPG",cv2.CV_LOAD_IMAGE_GRAYSCALE)
#cv2.imshow('hmm',image)
#cv2.waitKey()
#image=cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image,(5,5),0)
#cv2.imshow('blurred.png',blurred)
#cv2.waitKey()

kernelx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
kernely = np.array([[-1,-2,-1],[0,0,0],[1,2,1]])
gradientx = computeGradient(image,kernelx)
gradienty = computeGradient(image,kernely)
dim = gradientx.shape
edges = np.zeros(dim)
edges = np.add(np.square(gradientx), np.square(gradienty))
edges = np.sqrt(edges)
output = convertToBlackAndWhite(edges,192)


cv2.imwrite('img3-high.png',edges)